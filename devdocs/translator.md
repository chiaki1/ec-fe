## Note

- Sử dụng **react-i18next** hỗ trợ cho 2 kiểu SSR và CSR
- Có thể chia theo nhiều namespace. Thư mục chứa file translate theo namespace là: public/static/locales/{{lng}}/{{ns}}.json
- Có thể sử dụng **TRANSLATE** extension để translate text

```typescript
<UiExtension uiId="TRANSLATE" text="Your name" />
```

- Mặc định translate sẽ sử dụng common ns