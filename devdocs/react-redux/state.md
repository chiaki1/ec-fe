#### How to implement?

Lúc trước sử dụng immuableJS nhưng có một số điểm bất cập như:

- Phải khai báo thêm một kiểu record để typing
- Không thể lấy giá trị như native bằng mà phải qua function get
- Khi thao tác với giá trị thì lúc nào cũng phải quan tâm nó là immutableJS hay là native object

Chuyển sang sử dụng: https://github.com/mweststrate/immer

Redux by default cũng sử dụng luôn thư viện này nên khi mình thay đổi trong reducer không cần phải implement produce function