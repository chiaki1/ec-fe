#### How to add?

Tất cả đều call qua actions

```typescript
addProductsToCart({
  items: any[]
})
```

Với idea chung áp dụng cho toàn bộ các type(simple, configurable...) trong mỗi item có:

product và input. 

input là dữ liệu mà có thể truyền trực tiếp vào mutation graphql lúc call.

Ví dụ cho từng kiểu

##### Simple:



```typescript
{
    "items": [
        {
            "product": {
                "type": "simple",
                "__typename": "SimpleProduct"
            },
            "input": {
                "data": {
                    "quantity": 1,
                    "sku": "24-MG04"
                }
            }
        }
    ]
}
```

