Filter attribute:

- Attribute options:
    + Default là option-value
    + Mở rộng swatch: bao gồm type: (select = 0, visual(màu sắc) = 1, file(ảnh) = 2)
Thực tế thì swatch là loại mở rộng trong magento, bởi vì giá trị khi lưu vào vẫn có option_id và option_value. 
Cấu trúc như sau:

**eav_attribute_option**: Lưu các option của attribute, ví dụ như attribute `color` có `attribute_id` = 93 thì sẽ có các `option_id` trong này

**eav_attribute_option_value**: Lưu giá trị của option, mgt core chỉ quan tâm đến giá trị này  khi thực  hiện giao dịch. Mỗi một `option_id` sẽ có nhiều row là `value`

**eav_attribute_option_swatch**: Bảng này lưu thêm phần mở rộng là swatch. Lưu ý  khi  đẩy data vào  magento thì  đẩy option_value. Nhưng khi hiển thị magento  lấy giá trị  trong này. Tương ứng với `option_id` sẽ có 1 `swatch_id`. Swatch hiện tại có các kiểu như:(0 = text, 1=color, 2=image)

Vậy nên khi render ra thì swatch là  1 optional.  Hiển thị ra cho hợp mắt người dùng