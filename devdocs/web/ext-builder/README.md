#### Extension type

- Root
- Head
- Head component
- Component



#### Customize type

##### Hook

Kiểu hook sẽ define trước structure của component. User chỉ được phép chọn các component để add vào các điểm hook đã được khai báo sẵn.

Lưu ý: Các điểm hook đã được config trước UiID. Đây cũng là một điểm khác biệt cần lưu ý của hook.

