Một số điểm cần chú  ý:

- Cho đến thời điểm hiện tại, việc SSR chỉ được thực hiện duy nhất qua apollo graphql bằng cách sử dụng getInitialProps. 
Do đó các config SSR chỉ liên quan đến function này. 
- App support both of SPA and SSR
- Các Page export ra do không được load bằng dynamic nên sẽ được chạy luôn default export. Do đó cần phải export ra function

```javascript
// +pages/Bar.tsx
export default {
  PAGE: () => withAdapter(BarPage, { ssr: true }),
};
```

- Mặc định sẽ load theo SPA, trừ khi có config load = ssr. Ngoài ra sẽ có config ở root để force toàn bộ load = SPA(cho các server dùng chung). Nên lúc làm cần phải chú ý chạy được cả 2 trường  hợp
- Cần chú ý cách  lấy data từ apollo client. Nếu là ssr thì phải lấy ra data luôn  không được lưu vào state rồi mới lấy-> Do nếu lưu vào state cần đi qua  `effects` thì sẽ không  được render ra lúc trả về cho client

