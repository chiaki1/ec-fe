/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs-extra');

const chalk = require('chalk');
const execa = require('execa');
const figures = require('figures');
const path = require('path');
const chokidar = require('chokidar');
const lodash = require('lodash');

const warn = (msg, ...args) => {
  console.warn(`\n  ${figures.warning}  ${msg}\n`, ...args);
};
const rootDir = path.resolve(__dirname, '..');

const gracefulExit = () => {
  warn('Exiting watch mode.');
  process.exit(0);
};

process.on('SIGINT', gracefulExit);

let devServer;

const workspace = [
  {
    name: '@vjcspy/web-ui',
    path: 'packages/web-ui',
  },
  {
    name: '@vjcspy/web',
    path: 'packages/web',
  },
];

function startDevServer() {
  watchFileChange();

  const watch = [];
  workspace.forEach(({ name }) => {
    watch.push('yarn workspace ' + name + ' watch');
  });

  devServer = execa('concurrently', ['--kill-others', ...watch]);
  devServer.on('exit', () => {
    devServer.exited = true;
  });
  devServer.stdout.pipe(process.stdout);
  devServer.stderr.pipe(process.stderr);
}

const eventBuffer = [];

const watchFiles = [
  'packages/web-ui/src/**/*.{scss,css}',
  'packages/web-chiaki/src/**/*.{scss,css}',
  'packages/web-ui/public/**/*',
  'packages/web-chiaki/public/**/*',
];

const copyStyleSheet = async (moduleExisted) => {
  warn(`copy stylesheet in module ${chalk.yellow(moduleExisted.name)}`);
  const { stderr, stdout } = await execa('yarn', [
    'workspace',
    moduleExisted.name,
    'copyfiles',
  ]);

  if (stderr) {
    console.log(stderr);
  }

  if (stdout) {
    console.log(stdout);
  }
};

const copyPublicFolder = async (moduleExisted) => {
  fs.copy(
    moduleExisted.path + '/public',
    'packages/web/public',
    function callback(err) {
      if (err) throw err;
      warn(`copy public in module ${chalk.yellow(moduleExisted.name)}`);
    }
  );
};

async function whenFileChange() {
  let moduleRegex = new RegExp('(.*/?)(packages/[^/]*)(/{1})(.*)');
  const moduleChanged = lodash.reduce(
    eventBuffer,
    (modules, changes) => {
      const { name: typeChange, file: fileName } = changes;

      const moduleName = moduleRegex.exec(fileName);
      if (moduleName && moduleName.length === 5) {
        modules.push({
          module: moduleName[2],
          file: moduleName[4],
          typeChange,
        });
      }
      return modules;
    },
    []
  );

  let moduleNeedCopyStyle = [];
  let moduleNeedCopyPublic = [];
  if (moduleChanged.length > 0) {
    lodash.forEach(moduleChanged, ({ module, file }) => {
      const moduleExisted = lodash.find(workspace, (w) => w.path === module);

      if (!moduleExisted) {
        warn(`Not found module for path ${chalk.red(module)}`);
        return true;
      }
      if (file.indexOf('css') > -1) {
        moduleNeedCopyStyle.push(moduleExisted);
      }

      if (file.indexOf('public/') === 0) {
        moduleNeedCopyPublic.push(moduleExisted);
      }
    });

    moduleNeedCopyStyle = lodash.uniq(moduleNeedCopyStyle);
    moduleNeedCopyPublic = lodash.uniq(moduleNeedCopyPublic);

    moduleNeedCopyStyle.forEach((value) => copyStyleSheet(value));
    moduleNeedCopyPublic.forEach((value) => copyPublicFolder(value));

    setTimeout(() => (eventBuffer.length = 0), 500);
  }
}

function watchFileChange() {
  const eventsToListenTo = ['add', 'change', 'unlink'];
  const watcher = chokidar.watch(watchFiles, {
    ignored: '**/__*__/**/*',
  });

  const enqueue = (name, file) => {
    eventBuffer.push({ name, file });
    whenFileChange();
  };
  // chokidar appears not to have `.removeEventListener`, so this is the next
  // best thing: just reassign functions.
  let handler = lodash.debounce(() => {
    handler = enqueue;
  }, 900);
  //console.log(`File ${file} has been ${name}`)
  eventsToListenTo.forEach((name) =>
    watcher.on(name, (file) => handler(name, file))
  );
}

startDevServer();
