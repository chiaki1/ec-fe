const execa = require('execa');
const chalk = require('chalk');

const packages = [
  '@vjcspy/chitility',
  '@vjcspy/apollo',

  '@vjcspy/ui-extension',

  '@vjcspy/r',
  '@vjcspy/r-app',
  '@vjcspy/r-config',
  '@vjcspy/r-account',
  '@vjcspy/r-catalog',
  '@vjcspy/r-checkout',
  '@vjcspy/r-store',

  '@vjcspy/web-base',
  '@vjcspy/web-form',
  '@vjcspy/web-r',
  '@vjcspy/web-i18n',
  '@vjcspy/web-apollo',

  '@vjcspy/web-toastify',
  '@vjcspy/web-domain',
  '@vjcspy/web-store',
  '@vjcspy/web-url-rewrite',
  '@vjcspy/web-account',
  '@vjcspy/web-catalog',
  '@vjcspy/web-checkout',

  '@vjcspy/web-ui',
  '@vjcspy/web-chiaki',
];

const build = async () => {
  for (let i = 0; i < packages.length; i++) {
    console.info(`>> start build module ${chalk.yellow(packages[i])}`);
    const { stderr, stdout } = await execa('yarn', [
      'workspace',
      packages[i],
      'build',
    ]);

    if (stderr) {
      console.log(stderr);
    }

    if (stdout) {
      console.log(stdout);
    }
  }
};

build();
