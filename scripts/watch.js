const packagesFolder = './packages/';
const fs = require('fs');
const _ = require('lodash');
const chokidar = require('chokidar');
const chalk = require('chalk');
const figures = require('figures');
const execa = require('execa');
const { spawn } = require('child_process');

const warn = (msg, ...args) => {
  console.warn(`\n  ${figures.warning}  ${msg}\n`, ...args);
};

const retrievePackageModule = async () => {
  return new Promise((resolve) => {
    const modules = [];
    fs.readdir(packagesFolder, (err, webFolders) => {
      if (webFolders) {
        webFolders.forEach((webFolder) => {
          try {
            if (fs.existsSync(packagesFolder + webFolder + '/package.json')) {
              modules.push({
                name: '@vjcspy' + '/' + webFolder,
                folder: 'packages/' + webFolder,
              });
            }
          } catch (err) {
            console.error(err);
          }
        });
        resolve(modules);
      }
    });
  });
};
const retrieveListFileWatch = async (modules) => {
  const listWatchFiles = [];
  _.forEach(modules, (m) => {
    listWatchFiles.push(m.folder + '/' + 'src/**/*.{scss,css,ts,tsx}');
  });

  return listWatchFiles;
};

const eventBuffer = [];
const moduleRegex = new RegExp(
  '(.*\\/?)(packages\\/)([^\\/]*)(\\/{1})(.*)(\\.{1})(.*)'
);

const copyStyleSheet = async (moduleName) => {
  warn(`copy stylesheet in module ${chalk.yellow(moduleName)}`);
  const { stderr, stdout } = await execa('yarn', [
    'workspace',
    moduleName,
    'copyfiles',
  ]);

  if (stderr) {
    console.log(stderr);
  }

  if (stdout) {
    console.log(stdout);
  }
};
let tscWatched = [];
/*
 * Do thằng tsc chưa hỗ trợ compile từng file nếu sử dụng kèm với option nên
 * ở đây phải build lại toàn bộ, hơi chán
 * */
const rebuildSingleFile = async (moduleName, file) => {
  if (_.includes(tscWatched, moduleName)) {
    return;
  }
  warn(
    `module ${chalk.yellow(moduleName)} with file change ${chalk.yellow(
      file
    )} will start watch`
  );
  try {
    const tscProcess = spawn('yarn', [
      'workspace',
      moduleName,
      'tsc',
      '--project',
      'tsconfig.json',
      '-w',
      '--preserveWatchoutput',
    ]);

    tscProcess.on('close', (code) => {
      tscWatched = tscWatched.filter((d) => d !== moduleName);
    });
    tscProcess.stdout.on('data', (data) => {
      console.log(chalk.yellow(moduleName) + ': ' + data.toString());
    });

    tscProcess.stderr.on('data', (data) => {
      console.log(chalk.yellow(moduleName) + ': ' + data.toString());
    });
    tscWatched.push(moduleName);
  } catch (e) {
    console.error(e);
    tscWatched = tscWatched.filter((d) => d !== moduleName);
  }
};

const whenFileChange = () => {
  const moduleChanged = _.reduce(
    eventBuffer,
    (modules, changes) => {
      const { name: typeChange, file: fileName } = changes;

      const moduleName = moduleRegex.exec(fileName);
      if (moduleName && moduleName.length === 8) {
        modules.push({
          path: moduleName[0],
          module: moduleName[3],
          fileType: moduleName[7],
          relativePath: moduleName[5] + moduleName[6] + moduleName[7],
          typeChange,
        });
      }
      return modules;
    },
    []
  );
  let moduleNeedCopyStyle = [];
  let moduleNeedRebuildFile = [];
  _.forEach(moduleChanged, ({ module, fileType, relativePath }) => {
    if (_.includes(['tsx', 'ts'], fileType)) {
      moduleNeedRebuildFile.push({
        module: '@vjcspy/' + module,
        relativePath,
      });
    } else if (_.includes(['scss', 'css'], fileType)) {
      moduleNeedCopyStyle.push('@vjcspy/' + module);
    }
  });

  moduleNeedCopyStyle = _.uniq(moduleNeedCopyStyle);
  moduleNeedCopyStyle.forEach((value) => copyStyleSheet(value));
  moduleNeedRebuildFile.forEach((data) =>
    rebuildSingleFile(data.module, data.relativePath)
  );
};

const watch = async () => {
  const modules = await retrievePackageModule();
  const listWatchFiles = await retrieveListFileWatch(modules);

  const eventsToListenTo = ['add', 'change', 'unlink'];
  const watcher = chokidar.watch(listWatchFiles, {
    ignored: [
      '**/__*__/**/*',
      'packages/**/node_modules/**/*',
      'packages/**/build/**/*',
    ],
  });

  const enqueue = (name, file) => {
    eventBuffer.push({ name, file });
    whenFileChange();
    eventBuffer.length = 0;
  };
  // chokidar appears not to have `.removeEventListener`, so this is the next
  // best thing: just reassign functions.
  let handler = _.debounce(() => {
    console.log(chalk.green('watching file change...'));
    handler = enqueue;
  }, 1000);

  //console.log(`File ${file} has been ${name}`)
  eventsToListenTo.forEach((name) =>
    watcher.on(name, (file) => handler(name, file))
  );
};

watch();
